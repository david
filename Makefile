VERSION = 0.1.1
CC = gcc
CFLAGS = -O2 -Wall

all:
	$(CC) $(CFLAGS) -o david david.c

release:
	git archive --format=tar --prefix=david-$(VERSION)/ HEAD | \
	  gzip -9 > david-$(VERSION).tar.gz
